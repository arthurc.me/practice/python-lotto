import os
import random


DEBUG = os.environ.get('DEBUG') == 'True'


def log(*data):
    if not DEBUG:
        return

    print(*data)


def user_choose():
    print('請輸入 6 個不重複的數字, 數字須介於 1 與 49 之間。')

    result = []
    count = 1

    while count <= 6:
        number = input('請輸入您第 {} 幾個數字: '.format(count))
        if not number.isdigit():
            print('您輸入的不是一個數字。')
            continue

        number = int(number)

        if not (1 <= number <= 49):
            print('您輸入的數字不再 1 與 49 之間。')
            continue

        if number in result:
            print('您輸入的數字重複了')
            continue

        result.append(number)
        count += 1

    result = sorted(result)
    log('user input:', result)
    return result


def computer_choose():
    result = random.sample(range(1, 50), 6)
    result = sorted(result)
    log('computer choose:', result)
    return result


def check_result(target, choices):
    return len([i for i in choices if i in target])


def main():
    target = computer_choose()
    log('target:', target)

    print('您想要自已選擇數字還是由電腦幫您選擇？')
    choose_way = input('輸入 "y" 來自己選則，輸入其他讓電腦幫您選擇：')
    choices = (user_choose if choose_way == 'y' else computer_choose)()

    print('結果:')
    print('\t題目：{}'.format(', '.join([str(i) for i in target])))
    print('\t答案：{}'.format(', '.join([str(i) for i in choices])))
    print('\t您總共猜中 {} 個數字'.format(check_result(target, choices)))


if __name__ == '__main__':
    main()

